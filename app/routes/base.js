var express = require("express");
var router = express.Router();

router.get("/", function (req, res, next) {
    res.render("chat", { title: "Express" });
});

router.get("/chat", function (req, res, next) {
    res.render("chat", { title: "Express" });
});

router.get("/incidencias", function (req, res, next) {
    res.render("incidencias", { title: "Express" });
});

router.get("/listaIncidencias", function (req, res, next) {
    res.render("listaIncidencias", { title: "Express" });
});

router.get("*", function (req, res, next) {
    res.render("404", { title: "Express" });
});

module.exports = router;