require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");

var server = require("http").createServer(app).listen(port, () => {
    console.log("Felicidades tu app esta funcionando por el puerto:" +port)
});

app.use(express.static(__dirname + '/views'));

app.set("view engine","jade")


// bootstrap

app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/public/js', express.static(__dirname + '/public/js'));
app.use('/public/css', express.static(__dirname + '/public/css'));


// router

var indexRouter = require("./app/routes/base");

app.use('/',indexRouter)

